Godot 4 Seamless Hexagonal Tiling
==
Taken from the awesome tuts on YT by Ben Cloward: Numbers 45,46,47 and 48.

Start here: https://www.youtube.com/watch?v=hc6msdFcnA4

You will find a Visual Shader as well as a normal code shader. 

HTH
/dbat


![alt text](hextile_screenshot.jpg "Showing the Visual shader version on the left and the shader on the right.")
